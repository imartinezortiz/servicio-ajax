<?php

require_once __DIR__.'/includes/config.php'
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Portada</title>
</head>
<body>
    <h1>Peticiones AJAX</h1>
    <section class="op1">
      <h2>Peticiones simples (GET)</h2>
      <p>Genera una petición AJAX (GET) con resultado correcto: <button class="ok">OK</button></p>
      <p>Genera una petición AJAX (GET) con un error como resultado: <button class="error">Error</button></p>
    </section>
    <section class="op2">
      <h2>Peticiones avanzadas (GET/POST)</h2>
      <div>
        <p>Genera una petición AJAX (GET) con resultado correcto:</p>  
        <div>
          <label for="id">Id: <input id="id" type="number" min="1" max="10" value="0"/></label>
        </div>
        <div>
          <button class="carga">Obtiene</button>
        </div>
      </div>
      <div>
      <div>
        <p>Genera una petición AJAX (POST) con resultado correcto:</p>  
        <div>
          <!-- El formulario se utiliza para simplificar la recopilación de datos-->
          <form id="nuevo">
            <label for="msg">Id: <input id="msg" name="msg" type="text" value="Hola Don José" /></label>
          </form>
        </div>
        <div>
          <button class="add">Añade y Obtiene</button>
        </div>
      </div>
      <div>

      <div><pre><code class="resultado"></code></pre></div>
    </section>
    <script src="//code.jquery.com/jquery-3.6.0.min.js"   integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
    <script>
      $(function() {

        $(".op1 .ok").click(function() {
          $.get("servicio.php?op=op1&resultado=ok")
          .done(function (data, textStatus, jqXHR) {
            alert("Resultado: "+data.resultado);
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            alert("o_O no debería pasar");
          })
        });

        $(".op1 .error").click(function() {
          $.get("servicio.php?op=op1&resultado=error")
          .done(function (data, textStatus, jqXHR) {
            alert("o_O no debería pasar");
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            switch(jqXHR.status) {
              case 400 :
                alert("^_^ error de cliente: "+jqXHR.responseJSON.resultado);    
              break;
              default:
                alert("o_O código no esperado");
            }        
          })
        });

        $(".op2 .carga").click(function() {
          let id = $("#id").val();
          $.get("servicio.php?op=op2&id="+id)
          .done(function (data, textStatus, jqXHR) {
            $(".resultado").text(JSON.stringify(data));
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            switch(jqXHR.status) {
              case 400 :
                alert("^_^ error de cliente: "+jqXHR.responseJSON.resultado);    
              break;
              default:
                alert("o_O código no esperado");
            }        
          })
        });
        $(".op2 .add").click(function() {
          let msg = $('#msg').val();
          $.post("servicio.php?op=op2", {
            msg: msg,
            id: '25'
          })
          .done(function (data, textStatus, jqXHR) {
            $(".resultado").text(JSON.stringify(data));
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            switch(jqXHR.status) {
              case 400 :
                alert("^_^ error de cliente: "+jqXHR.responseJSON.resultado);    
              break;
              default:
                alert("o_O código no esperado");
            }        
          })
        });
      })
    </script>
  </body>
</html>
