<?php

require_once __DIR__.'/includes/config.php';

// XXX Ñapa, simulando una BD.
if (! isset($_SESSION['BD']) ) {
  $_SESSION['BD'] = [];
    /* stdClass es una clase predefinida de PHP que no tiene propiedades ni métodos
     * https://www.php.net/manual/en/reserved.classes.php
    */
  $mensaje = new stdClass();
  $mensaje->id = 1;
  $mensaje->mensaje = 'Hola Don Pepito';
  $_SESSION['BD'][] = $mensaje;
}

// LE indica al navegador que la respuesta será un objeto JSON
header('Content-Type: application/json; charset=utf-8');


/*
 * La operación 1 sólo acepta peticiones GET
 */
function operacion1() {
  switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
      $id = $_GET['resultado'] ?? ''; 
      if ( $id === 'ok' ) {
        /* stdClass es una clase predefinida de PHP que no tiene propiedades ni métodos
        * https://www.php.net/manual/en/reserved.classes.php
        */
        $result = new stdClass();
        $result->resultado = 'Resultado correcto';
        echo json_encode($result);
      } else {
        /* stdClass es una clase predefinida de PHP que no tiene propiedades ni métodos
        * https://www.php.net/manual/en/reserved.classes.php
        */
        $result = new stdClass();
        $result->resultado = 'Resultado incorrecto';
        http_response_code(400);
        echo json_encode($result);
      }
    break;
    default:
      $error = new stdClass();
      $error->error = "No se aceptan peticiones: $_SERVER[REQUEST_METHOD]";
      http_response_code(400);
      echo json_encode($error);
    break;
  } 
}

/*
 * La operación 2 acepta peticiones GET y POST 
 */
function operacion2() {
  switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $id = $_GET['id'] ?? '';
        /* stdClass es una clase predefinida de PHP que no tiene propiedades ni métodos
        * https://www.php.net/manual/en/reserved.classes.php
        */
        $mensaje = $_SESSION['BD'][$id] ?? null;
        if (!$mensaje) {
          $error = new stdClass();
          $error->error = "No existe el mensaje con id: $id";
          http_response_code(400);
          echo json_encode($error);
          return;
        }
        echo json_encode($mensaje);
      break;
    case 'POST':
        /* stdClass es una clase predefinida de PHP que no tiene propiedades ni métodos
        * https://www.php.net/manual/en/reserved.classes.php
        */
        $msg = filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_SPECIAL_CHARS);
        $mensaje = new stdClass();
        $mensaje->id = count($_SESSION['BD'])+1;
        $mensaje->mensaje = $msg;
        $_SESSION['BD'][] = $mensaje;
        echo json_encode($mensaje);
      break;
    default:
      $error = new stdClass();
      $error->error = "No se aceptan peticiones: $_SERVER[REQUEST_METHOD]";
      http_response_code(400);
      echo json_encode($error);
    break;
  }
}


$op = filter_input(INPUT_GET, 'op', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
switch($op) {
  case 'op1':
    operacion1();
  break;
  case 'op2':
    operacion2();
  break;
  default:
    $error = new stdClass();
    $error->error = "Operación no soportada: $op";
    http_response_code(400);
    echo json_encode($result);
  break;
}
